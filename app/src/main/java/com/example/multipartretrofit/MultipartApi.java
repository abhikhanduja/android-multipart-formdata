package com.example.multipartretrofit;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class MultipartApi {
    public static final String url = "http://115.124.98.243/~guruji/api/";
    public static RequestMethods methods = null;

    public static RequestMethods getService() {
        if (methods == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
            methods = retrofit.create(RequestMethods.class);
        }
        return methods;
    }

    public interface RequestMethods {
        @Headers({"auth-token: f75t1uie5jvzuqc851cjmlv38khjri6xbx16l2m5ymx6yznqdkh4awdi3u8pt3n69aml5bokw6arrlhvds7porqkc4djf1682m6k"})
        @Multipart
        @POST("tutorapi/uploaddocument/format/json")
        Call<ResponseData> uploadAttachment(@Part @Nullable MultipartBody.Part resume,
                                            @Part @Nullable MultipartBody.Part educationdoc,
                                            @Part("doctype") RequestBody doctype);

    }
}