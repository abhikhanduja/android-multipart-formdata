package com.example.multipartretrofit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final int PICK_IMAGE = 100;

    TextView tvDoc, tvResume, tvUpload;

    Uri uri = null;

    File docFile = null, imageFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvDoc = findViewById(R.id.tvDoc);
        tvResume = findViewById(R.id.tvResume);
        tvUpload = findViewById(R.id.tvUpload);
        tvDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        tvResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
            }
        });
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    5);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public MultipartBody.Part getMultipartFile(String key, File file, String mimeType) {
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(key, file.getName(), RequestBody.create(MediaType.parse(mimeType), file));
        return filePart;
    }

    public void getData() {
        MultipartBody.Part image = null;
        MultipartBody.Part resume = null;

        HashMap<String, String> map = new HashMap<>();

        if (imageFile != null && docFile != null) {
            map.put("doctype", "Both");
        }else if (imageFile != null) {
            image = getMultipartFile("educationdoc", imageFile, "image/jpeg");
            map.put("doctype", "Document");
        }else if (docFile != null) {
            resume = getMultipartFile("resume", docFile, "application/pdf");
            map.put("doctype", "Resume");
        }else {
            return;
        }

//        image = getMultipartFile("educationdoc", imageFile, "image/jpeg");
//        resume = getMultipartFile("resume", docFile, "application/pdf");

        RequestBody doctype = RequestBody.create(MediaType.parse("multipart/form-data"), "Resume");

        List<MultipartBody.Part> mediaArray = new ArrayList<>(Arrays.asList(image));

        Call<ResponseData> runRequest = MultipartApi.getService().uploadAttachment(resume, image, doctype);

        runRequest.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                Log.d("12321", "onResponse: " + response.toString());
                ResponseData responseData = response.body();

                Toast.makeText(MainActivity.this, responseData.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Log.d("12321", "onResponse: " + t.getMessage());
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permissions, 20);
            } else {
                openGallery();
            }
        } else {
            openGallery();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            uri = data.getData();
            File file = new File(getRealPathFromURI(uri));
            imageFile = file;
//            getData(file);
        }

        if (requestCode == 5) {
            if (resultCode == RESULT_OK) {
                uri = data.getData();
                File file_pdf = new File(uri.getPath());
                final String[] split = file_pdf.getPath().split(":");
                String filepath = split[1];
                docFile = new File(filepath);
//                getData(file_pdf);
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

//    public String getPath(Uri uri) {
//        String[] projection = {MediaStore.Images.Media.DATA};
//        Cursor cursor = managedQuery(uri, projection, null, null, null);
//        startManagingCursor(cursor);
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        cursor.moveToFirst();
//        return cursor.getString(column_index);
//    }

}