package com.example.multipartretrofit;

import okhttp3.MultipartBody;

public class FormDataModel {

    enum MimeTypes {
        image, pdf, doc, docx
    }

    String key = null;
    MultipartBody.Part data = null;
    String filename = null;
    String mimeType = null;

    public FormDataModel(String key, MultipartBody.Part data, String filename, MimeTypes mimeType) {
        this.key = key;
        this.data = data;
        this.filename = filename;

        if (mimeType == MimeTypes.image) {
            this.mimeType = "image/jpeg";
        }else if (mimeType == MimeTypes.pdf) {
            this.mimeType = "application/pdf";
        }else if (mimeType == MimeTypes.doc) {
            this.mimeType = "application/msword";
        }else if (mimeType == MimeTypes.docx) {
            this.mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        }

    }
}
